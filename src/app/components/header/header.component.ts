import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public readonly title = 'Observables - a better way';
  public amount = 0;
  @Output() toggle: EventEmitter<any> = new EventEmitter();

  private subscription = new Subscription();

  constructor(
    private _messageService: MessageService
  ) { }

  ngOnInit() {
    this.subscription.add(this._messageService.data().subscribe(page => {
      this.amount = page.metadata.warning;
    }));
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

  public toggleSidenav() {
    this.toggle.emit();
  }

  public logout() {
    alert('Sorry, you can\'t logout. We\'ve got you entrapped');
  }

  public refresh() {
    this.subscription.add(this._messageService.refreshData()
      .subscribe());
  }
}
