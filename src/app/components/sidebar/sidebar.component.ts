import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {

  public notRead = 0;

  private subscription = new Subscription();

  constructor(
    private _messageService: MessageService
  ) { }

  ngOnInit() {
    this.subscription.add(this._messageService.data()
      .subscribe(page => {
        this.notRead = page.metadata.total - page.metadata.read;
      }));
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }
}

