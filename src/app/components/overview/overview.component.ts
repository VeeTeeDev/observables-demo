import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Message } from '../../models/Message';
import { MessagePage } from 'src/app/models/message-page';
import { tap } from 'rxjs/operators';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  public readonly displayedColumns = ['read', 'name', 'email', 'title', 'message', 'priority'];

  public page: MessagePage = new MessagePage();
  public dataSource = new MatTableDataSource<Message>();

  private subscription = new Subscription();

  constructor(
    private _messageService: MessageService
  ) { }

  ngOnInit() {
    this._messageService.data()
      .subscribe((page: MessagePage) => {
        if (page.data !== undefined) {
          this.dataSource.data = page.data as Message[];
        }
        if (page.metadata !== undefined) {
          this.page.metadata.total = page.metadata.total;
          this.page.metadata.count = page.metadata.count;
        }
        if (page.paging !== undefined) {
          this.page.paging.size = page.paging.size;
          this.page.paging.index = page.paging.index;
        }
      });
  }

  ngAfterViewInit() {
    this.subscription.add(this.paginator.page
      .pipe(
        tap(() => {
          this.page.paging.size = this.paginator.pageSize;
          this.page.paging.index = this.paginator.pageIndex;
          this._messageService.setPage(this.page);
          this._messageService.reload();
        })
      ).subscribe());
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

  rowClicked(message: Message) {
    if (!message.read) {
      message.read = true;
      this.subscription.add(this._messageService.update(message)
        .subscribe());
    }
  }
}
