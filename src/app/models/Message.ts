export class Message {
    id: number;
    name: string;
    email: string;
    title: string;
    message: string;
    priority: boolean;
    read: boolean;
    date: Date;
}
