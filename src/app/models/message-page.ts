import { Metadata } from './message-metadata';
import { Paging } from './message-paging';
import { Message } from './Message';

export class MessagePage {
    metadata: Metadata;
    data: Array<Message>;
    paging: Paging;

    constructor() {
        this.paging = new Paging();
        this.metadata = new Metadata();
        this.data = [];
    }
}
