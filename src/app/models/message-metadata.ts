export class Metadata {
    read: number;
    count: number;
    priority: number;
    warning: number;
    total: number;
}
