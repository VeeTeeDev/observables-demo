import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { MessagePage } from '../models/message-page';

import * as queryString from 'query-string';
import { MessageFilter } from '../models/message-filter';
import { Message } from '../models/Message';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private _data$: Subject<MessagePage> = new BehaviorSubject<MessagePage>(new MessagePage());
  private _data: MessagePage = new MessagePage();

  private _filter: MessageFilter = new MessageFilter();
  private _filterChanged = false;

  private _page: MessagePage = new MessagePage();
  private _pageChanged = false;

  private _loaded = false;
  private _loading = false;

  private _eventSource: EventSource;

  constructor(
      private _http: HttpClient,
      private _ngZone: NgZone
  ) {
    this._eventSource = new EventSource('/api/stream');
    this._eventSource.onmessage = this._onMessage.bind(this);
  }

  private _onMessage(ev: MessageEvent): void {
    this._ngZone.run(() => {
      this._page.metadata = JSON.parse(ev.data);
      const page: MessagePage = {
        metadata: JSON.parse(ev.data),
        data: undefined,
        paging: undefined
      };
      this._data$.next(page);
    });
  }

  public data(): Observable<MessagePage> {
    if (!this._loaded) {
      this._filterChanged = true;
      this.reload();
      this._loaded = true;
    }
    return this._data$.asObservable();
  }

  public update(message: Message): Observable<any> {
    return this._http.post(`/api/message/${message.id}`, message);
  }

  public refreshData(): Observable<any> {
    return this._http.get('/api/refresh')
      .pipe(
        tap(() =>  {
            this._pageChanged = true;
            this.reload();
        })
      );
  }

  public reload() {
    if (this._filterChanged || this._pageChanged || !this._loading) {
      this._loading = true;
      this._http.get<MessagePage>(
        `/api/message/?${queryString.stringify({...{index: this._page.paging.index, size: this._page.paging.size}, ...this._filter})}`
      ).subscribe(page => {
          this._data = page;
          this._loading = false;
          this._filterChanged = false;
          this._pageChanged = false;
          this._data$.next(page);
        }, () => {
          this._loaded = false;
        });
    } else {
      this._data$.next(this._data);
    }
  }

  public setPage(page: MessagePage) {
    this._pageChanged = true;
    this._page.paging.size = page.paging.size;
    this._page.paging.index = page.paging.index;
    this.reload();
  }

  public setFilter(filter: MessageFilter) {
    this._filter = filter;
    this._filterChanged = true;
    this.reload();
  }

  public reset() {
    this._filter = new MessageFilter();
    this._filterChanged = true;
    this.reload();
  }

  public clear() {
    this._data = new MessagePage();
    this._data$.next(this._data);
  }

}
