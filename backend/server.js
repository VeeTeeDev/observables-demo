const express = require('express');
const bodyParser = require('body-parser');
const jsonLoader = require('json-file-loader')
const app = express();

const sse_middleware = (req, res, next) => {
    res.sseSetup = () => {
        req.socket.setTimeout(0);
        req.socket.setNoDelay(true);
        req.socket.setKeepAlive(true);
        res.setHeader('Content-Type', 'text/event-stream');
        res.setHeader('Cache-Control', 'no-cache');
        res.setHeader('Connection', 'keep-alive');
        res.statusCode = 200;   
    }
  
    res.sseSend = (data) => {
        res.write('data:' + JSON.stringify(data) + '\n\n');
    };

    res.sseOnClose = (callback) => {
        req.on('onClose', callback);
    };
    
    next();
}

let data = [];
let channels = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(sse_middleware);

app.get('/', (_, res) => {
    res.status(200).send('Welcome to our restful API');
});

app.get('/stream', (_, res) => {
    let identifier = channels.length;
    channels.push(res);
    res.sseSetup();
    res.sseOnClose(()=> {
        delete channels[identifier];
    })
});

app.get('/refresh', (_, res) => {
    for (let message of data) {
        message.priority = Math.random() > 0.9;
        message.read = Math.random() > 0.2;
        message.warning = message.priority && !message.read;  
    }
    res.status(200).send({});
});

app.get('/message', (req, res) => {
    const filter = {
        name: req.query.name,
        title: req.query.title,
        message: req.query.message
    };
    const index = Number(req.query.index);
    const size = Number(req.query.size);
    const page = new Page(index, size);

    if (!filter.name && !filter.title && !filter.message) {
        const filtered = data.slice(page.paging.index * page.paging.size, (page.paging.index * page.paging.size) + page.paging.size);
        page.data = filtered;
        page.metadata.count = filtered.length;
    } else {
        const filtered = data.filter((element) => {
            if (filter.name && element.name.indexOf(filter.name) > -1) {
                return true;
            }
            if (filter.title && element.title.indexOf(filter.title) > -1) {
                return true;
            }
            if (filter.message && element.message.indexOf(filter.message)  > -1 ) {
                return true;
            }
            return false;        
        });
        filtered.splice(page.paging.index * page.paging.size, (page.paging.index * page.paging.size) + page.paging.size);
        page.metadata.count = filtered.length;
        page.data = filtered;
    }
    res.status(200).send(page);  
});

app.get('/message/:id', (req, res) => {
    const messageId = req.params.id && parseInt(req.params.id);
    if (messageId !== undefined) {
        res.status(200).send(findById(messageId));
    } else {
        res.status(400).send('id not found');
    }
});

app.post('/message/:id', (req, res) => {
    updateMessage(req.body);
    res.status(200).send({});  
    notifyClient();
});

jsonLoader.readFile('./backend/data.json')
    .then((jsondata) => {
        data = jsondata;
        for (let i in data) {
            data['id'] = i;
        }
    })
    .catch((error) => {
        console.log(error);
    });

const server = app.listen(3000, () => {
    console.log('Message service running on ', server.address().port);
});

const findById = (id) => {
    return data.find(message => message.id === id);
}

const notifyClient = () => {
    console.log(channels.length);
    for (let index in channels) {
        if (channels.hasOwnProperty(index)) {
            channels[index].sseSend(new Metadata());
        }
    }
}

const updateMessage = (message) => {
    const messageIndex = data.findIndex(m => m.id === message.id)
    if (messageIndex !== undefined) {
        data[messageIndex] = message;
    }
}

class Page {
    constructor(index, size) {
        this.data = [];
        this.paging = new Paging(index, size);
        this.metadata = new Metadata();
    }
}

class Paging {
    constructor(index, size) {
        this.index = index || 0;
        this.size = size || 20;
    }
}

class Metadata {
    constructor() {
        this.read = data.filter((el) => el.read).length;
        this.priority = data.filter((el) => el.priority).length;
        this.count = data.length;
        this.total = data.length;
        this.warning = data.filter((el) => el.priority && !el.read).length;
    }
}
